using FFTW
using DelimitedFiles
using Base.Threads
using TimerOutputs

function InitialCondition(x, rho0)
    Gaussian(x, center, width, rho0)
end

function Gaussian(x, center, width, rho0)
    #L = x[end]
    PsiP = rho0 .+ exp.(-(x.-center).^2 / (2*width^2) ) / sqrt(2pi*width^2)
    PsiP
end

function Noise!(PsiP, Amp, k, kICmin, kICmax, rho0)
    """
    Condensate of density rho0 perturbed by waves with amplitud Amp and 
    wavenumbers between kICmin and kICmax.
    """
    PsiP .= rho0
    PsiS = plan_rc*PsiP
    for i in axes(PsiS,1)
        if (abs(k[i])>=kICmin && abs(k[i]) <= kICmax)
            PsiS[i] += Amp*exp(1im*rand(1)[1])
        end
    end
    dealias!(PsiS)
end

function Ground_state!(PsiP, rho0)
    PsiP .= rho0
end

function soliton!(PsiS, center, width, vel)

    PsiP = plan_cr*PsiS

    for i in axes(PsiP, 1)
        # xper = 2*sin( (x[i] - center ) / (2) )
        xper = (x[i] - center )
        # Amplitud = sqrt(rho0)*sqrt((vel/c)^2/2 + (1 - (vel/c)^2/2)* tanh( xper / (sqrt(2)*width*xi) )^2 ) 
        Amplitud = sqrt(rho0)*sqrt((vel/c)^2 + (1 - (vel/c)^2)* tanh( (1-(vel/c)^2) * xper / (sqrt(2)*width*xi) )^2 ) 
        # Phase = atan( (2*vel^2 - vel^4)^(1/2) / ( exp( sqrt(2-vel^2) * xper) + vel^2 - 1 ) )
        Phase = atan( (4*(vel/c)^2 - 4*(vel/c)^4)^(1/2) / ( exp( sqrt(2-2*(vel/c)^2) * xper/(width*xi)) + 2*(vel/c)^2 - 1 ) )
        # Phase = 0
        #Phase = vel*x[i]
        # if xper<0
        #     Phase = 2*atan( (4*(vel/c)^2 - 4*(vel/c)^4)^(1/2) / ( 2*(vel/c)^2 - 1 ) ) 
        #            - atan( (4*(vel/c)^2 - 4*(vel/c)^4)^(1/2) / ( exp( sqrt(2-2*(vel/c)^2) * (-xper)/ (width*xi)) + 2*(vel/c)^2 - 1 ) )  
        # else
        #     Phase =   atan( (4*(vel/c)^2 - 4*(vel/c)^4)^(1/2) / ( exp( sqrt(2-2*(vel/c)^2) * xper/ (width*xi)) + 2*(vel/c)^2 - 1 ) )
        # end
        while Phase<0
           Phase = Phase + pi
        end
        PsiP[i] = PsiP[i] + Amplitud*exp(1im*Phase)
        if vel==0
            if i<N÷2
               PsiP[i] = -PsiP[i]
            end
        end
    end

    # PsiP = PsiP[1:end] .* PsiP[end:-1:1]

    PsiS = plan_rc*PsiP
    PsiS
end

function two_solitons!(PsiS, center, width, vel)

    PsiP = plan_cr*PsiS

    for i in axes(PsiP, 1)
        xper = (x[i] - center )
        Amplitud = sqrt(rho0)*sqrt((vel/c)^2 + (1 - (vel/c)^2)* tanh( (1-(vel/c)^2) * xper / (sqrt(2)*width*xi) )^2 ) 
        Phase = atan( (4*(vel/c)^2 - 4*(vel/c)^4)^(1/2) / ( exp( sqrt(2-2*(vel/c)^2) * xper/(width*xi)) + 2*(vel/c)^2 - 1 ) )
        while Phase<0
           Phase = Phase + pi
        end
        PsiP[i] = PsiP[i] + Amplitud*exp(1im*Phase)
        if vel==0
            if i<N÷2
               PsiP[i] = -PsiP[i]
            end
        end
    end

    PsiP = PsiP[1:end] .* PsiP[end:-1:1]

    PsiS = plan_rc*PsiP
    PsiS
end
