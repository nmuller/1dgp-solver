import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import os

N = 201 # Resolution
cmap = plt.cm.viridis
colors = [cmap( (i+1) / N) for i in range(N)]
path = 'outs/'
#path = 'restart/'
index = 0
files = os.listdir(path)
T = len(files) // 2

def load_density(path, index):
    Re = np.loadtxt(path+"ReaPsi_"+str(index).zfill(3)+".txt")
    Im = np.loadtxt(path+"ImaPsi_"+str(index).zfill(3)+".txt")
    D = Re**2 + Im**2
    return D

def load_phase(path, index):
    Re = np.loadtxt(path+"ReaPsi_"+str(index).zfill(3)+".txt")
    Im = np.loadtxt(path+"ImaPsi_"+str(index).zfill(3)+".txt")
    phase = np.angle(Re + 1j*Im)
    return phase
    
L = 2*np.pi
x = np.linspace(0, L, N)
#t = np.loadtxt(path+'time.txt')
k = np.fft.fftfreq(N)*N

#filenames = os.listdir(path)
#filenames.sort()
#filenames = filenames[1:]
D = load_density(path, index)
phase = load_phase(path, index)

fig, ax = plt.subplots(1,2,figsize=(10,6))
#ax.set(xlim=(x[0],x[-1]), ylim=(-3e-1, np.max(v)*1.1))

line = ax[0].plot(x,D)[0]
line = ax[1].plot(x,phase)[0]
#vf = np.fft.fft(v)
#line = ax.plot(k,vf)[0]

plt.show()
