
# Restart
restart = 0
path_out = "outs/"
path_in = "restart/"
SOLVER="GP"

# Global parameters
L = 2pi
N = 201
c = 1
nxi = 8
rho0 = 1
dt = 1.e-3
nsteps = 30000
nsave = 300
#nu = 0.1

# Definitions
dx = L/N
#x = Array(0:dx:L-dx)
x = Array(-L/2:dx:L/2-dx)
t = 0:dt:dt*nsteps
xi = dx*nxi
alpha = c*xi/sqrt(2)
k = fftfreq(N)*N
kmax = N÷3

# IC parameters
# Noise
Amp = 0
kICmin = 1
kICmax = 10
# Potential
AmpPot = 0
# Gaussian
center = -pi/2
width = 1
U = 0.3
#@assert dt < dx^2 / (4*nu)
