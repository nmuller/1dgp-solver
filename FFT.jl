using FFTW
using DelimitedFiles
using Base.Threads
using TimerOutputs

function init_fft(v)
    # FFTW.set_num_threads(4)
    plan_rc = plan_fft(v,  flags=FFTW.MEASURE, timelimit=Inf)
    plan_cr = plan_ifft(v,  flags=FFTW.MEASURE, timelimit=Inf)
    plan_rc, plan_cr
end
