import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import os

N = 201 # Resolution
cmap = plt.cm.viridis
colors = [cmap( (i+1) / N) for i in range(N)]
path = 'outs/'
files = os.listdir(path)
T = len(files) // 2

def load_density(path, index):
    Re = np.loadtxt(path+"ReaPsi_"+str(index).zfill(3)+".txt")
    Im = np.loadtxt(path+"ImaPsi_"+str(index).zfill(3)+".txt")
    D = Re**2 + Im**2
    return D
    
def load_phase(path, index):
    Re = np.loadtxt(path+"ReaPsi_"+str(index).zfill(3)+".txt")
    Im = np.loadtxt(path+"ImaPsi_"+str(index).zfill(3)+".txt")
    phase = np.angle(Re + 1j*Im)
    return phase

L = 2*np.pi
#x = np.linspace(0, L, N)
x = np.linspace(-np.pi, -np.pi+L, N)
t = np.loadtxt(path+'time.txt')
k = np.fft.fftfreq(N)*N

#filenames = os.listdir(path)
#filenames.sort()
#filenames = filenames[1:]
D = load_density(path, 0)
phase = load_phase(path, 0)

fig, ax = plt.subplots(figsize=(8,6))
ax.set(xlim=(x[0],x[-1]), ylim=(0, np.max(D)*1.1))

line = ax.plot(x,D)[0]
#line = ax.plot(x,phase)[0]
#vf = np.fft.fft(v)
#line = ax.plot(k,vf)[0]

nskip = 1
def animate(i):
    j = nskip*i
    D = load_density(path, j)
#    phase = load_phase(path, j)
    Rho = np.sum(D) / N
    #vf = np.fft.fft(v)
#    line.set_ydata(phase)
    line.set_ydata(D)
    ax.set_title(r'$t=%.1f \xi/c$ ; $|\psi|^2$=%.2f'%(t[j], Rho))

anim = FuncAnimation(fig, animate, interval=50, frames=(T-1)//nskip, repeat=True)

plt.draw()
plt.show()
