using FFTW
using Base.Threads

function NL(PsiS; timer=TimerOutput())
    #PsiP = @timeit timer "fft" plan_cr*PsiS
    PsiP = plan_cr*PsiS
    DenP = conj.(PsiP).*PsiP
    # PsiP = @timeit timer "product" ( -1 .+ DenP/rho0 ).*PsiP * c/(xi*sqrt(2.))
    # Psi_NLS = @timeit timer "fft" plan_rc*PsiP
    # @timeit timer "dealias!" dealias!(Psi_NLS)
    PsiP = ( -(1 .- Vext) .+ DenP/rho0 ).*PsiP * c/(xi*sqrt(2.))
    Psi_NLS = plan_rc*PsiP
    dealias!(Psi_NLS)
    Psi_NLS 
end

function DNL(PsiS)
    PsiP = plan_cr*PsiS
    # DenP = conj.(PsiP).*PsiP
    # @timeit timer "dealias!" dealias!(Psi_NLS)
    PsiP = ( -(1 .- Vext) .+ 2*conj(PsiP).*PsiP.*Psi1P/rho0 .+ PsiP.*PsiP.*conj(Psi1P)/rho0 ) * c/(xi*sqrt(2.))
    Psi_NLS = plan_rc*PsiP
    dealias!(Psi_NLS)
    Psi_NLS 
end

function RHS(PsiS; timer=TimerOutput())
    Psi_NLS = @timeit timer "NL" NL(PsiS; timer=timer)
    laplacian = @timeit timer "Lap" Lap(PsiS)
    Psi_NLS = @timeit timer "RHS" -1im*( -laplacian*alpha + Psi_NLS )  # GP
#    if SOLVER=="GP"
#        Psi_NLS = @timeit timer "RHS" -1im*( -laplacian*alpha + Psi_NLS )  # GP
#    elseif SOLVER=="RGL"
    if SOLVER=="RGL"
        PsiSx = @timeit timer "Dx" Dx(PsiS)
#        Psi_NLS = @timeit timer "RHS" ( - ( -laplacian*alpha + Psi_NLS ) - 1im*U*PsiSx )  # RGL
        Psi_NLS = @timeit timer "RHS" ( -1im*Psi_NLS - 1im*U*PsiSx )  # RGL
    elseif SOLVER=="AGP"
        PsiSx = @timeit timer "Dx" Dx(PsiS)
#        Psi_NLS = @timeit timer "RHS" -1im*( -laplacian*alpha + Psi_NLS ) + U*PsiSx  # ADV GP
        Psi_NLS = @timeit timer "RHS" Psi_NLS + U*PsiSx  # ADV GP
    # elseif SOLVER=="NEWTON"
    #     PsiSx = @timeit timer "Dx" Dx(PsiS)
    #     Psi_NLS = @timeit timer "DNL" DNL(PsiS)  # NEWTON 
    #     Psi_NLS = @timeit timer "RHS" Psi_NLS - laplacian*alpha - 1im*U*PsiSx  # NEWTON as in RGL
    end
    Psi_NLS
end

function dealias(PsiS)
   PsiS = Kill .* PsiS
end

function dealias!(PsiS)
   PsiS .= Kill .* PsiS
end

function init_dealias(k)
    Kill = similar(k)
    Kill[ abs.(k) .>= 2*kmax/3] .= 0
    Kill[ abs.(k) .<  2*kmax/3] .= 1
#    Kill[ abs.(k) .>= N/3 ] .= 0
#    Kill[ abs.(k) .<  N/3 ] .= 1
    Kill
end

function UpdatePotential!(Vext, n)
    for i in axes(Vext,1)
        potential_center = U*t[n] + center
        xper = 2*sin( (x[i] - potential_center ) / (2) )
        #xp = x[i] - potential_center
        Vext[i] = AmpPot * exp( - xper^2 / (2*(width*xi)^2) ) / sqrt(2pi*(width*xi)^2) + 0 * 1im
    end
end

function check_periodicity(xx, L)

    while xx > L
        xx = xx - L
    end

    while xx < 0
        xx = xx + L
    end

    xx
end


