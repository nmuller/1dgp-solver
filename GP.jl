using FFTW
using DelimitedFiles
using Base.Threads
using TimerOutputs

"""
One-dimensional GP Solver.
Pseudospectral code with RK4 to evolve in time.
It allows different solvers like GP, RGL and AGP.
"""

FFTW.set_num_threads(1)

include("parameters.jl")
include("FFT.jl")
include("TimeSchemes.jl")
include("Spectral.jl")
include("Pseudospectral.jl")
include("IC.jl")
include("io.jl")

function main(PsiS)
    PsiS1 = Array{ComplexF64}(undef, N)
    PsiS2 = Array{ComplexF64}(undef, N)
    PsiS3 = Array{ComplexF64}(undef, N)
    PsiS4 = Array{ComplexF64}(undef, N)
    for n=1:nsteps
#        @timeit to "Euler!" Euler!(PsiS, dt; timer=to)
#        @timeit to "RK2" RK2!(PsiS, dt; timer=to)
        @timeit to "RK4" RK4!(PsiS, PsiS1, PsiS2, PsiS3, PsiS4, dt; timer=to)
#        @timeit to "RK4" RK4!(PsiS, dt; timer=to)
        if SOLVER=="GP"
            UpdatePotential!(Vext, n)
        end
        if mod(n,nsave)==0
            E = @timeit to "compute energy" compute_energy(PsiS)
            @timeit to "save" SaveEnergy(E)
            @timeit to "save" SaveOutputs(PsiS, n, nsave)
        end
    end
end

function Allocate(N)
    PsiP  = Array{ComplexF64}(undef, N)
    PsiS = Array{ComplexF64}(undef, N)
    PsiP, PsiS
end
const to = TimerOutput()

Kill = init_dealias(k)
PsiP, PsiS = @timeit to "Allocation" Allocate(N)
# Vext = AmpPot * exp.( - (x .- center).^2 / (2*(width*xi)^2) ) / sqrt(2pi*(width*xi)^2) .+ 0 * 1im
Vext = zeros(Complex, N)
#Vext[N÷2] = sqrt(2)*(1-U^2/2)^(3/2) * tanh(sqrt(0.5-U^2/4)*center) / (U^2/2 + sinh(sqrt(0.5-U^2/4)*center))
#Vext[N÷2] = AmpPot
plan_rc, plan_cr = @timeit to "init fftw" init_fft(PsiS)

if restart==0
#    PsiS = @timeit to "IC" Noise!(PsiP, Amp, k, kICmin, kICmax, rho0)
    # PsiS = @timeit to "IC" soliton!(PsiS, center, width, U)
    # PsiS = @timeit to "IC" soliton!(PsiS, center, width, U)
    PsiS = @timeit to "IC" two_solitons!(PsiS, center, width, U)
    # PsiS = @timeit to "IC" soliton!(PsiS, center+L/2, width, U)
#    PsiS = @timeit to "IC" soliton(pi, 1, U)
else
    ReaPsi = readdlm(path_in*"ReaPsi_"*lpad(restart,3,"0")*".txt", Float64)
    ImaPsi = readdlm(path_in*"ImaPsi_"*lpad(restart,3,"0")*".txt", Float64)
    PsiP = ReaPsi + 1im*ImaPsi
    PsiP = PsiP[:, 1]
    PsiS = plan_rc*PsiP
end

#@timeit to "IC" Ground_state!(PsiP, rho0)
#PsiP = @timeit to "IC" Gaussian(x, center, width, rho0)
#PsiS = plan_rc*PsiP
#dealias!(PsiS)

# Save IC
SaveOutputs(PsiS, 0, nsave)

# Start evolution
@timeit to "main" main(PsiS)

print_timer(to)
