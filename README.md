# One-dimensional Gross-Pitaevskii solver

This code is a solver for one-dimensional superfluids. It solves the following equations:

- Gross-Pitaevskii (GP)
- Real Ginzburg-Landau (RGL)
- Advected Gross-Pitaevskii (AGP)


# Installation

TODO

# How to use

To run the code, simply execute the command:

`julia GP.jl`

`GP.jl` is the main file and here you choose the initial condition. 
The file `parameters.jl` set the parameters of the simulation. Here you can choose the solver.

