using FFTW
using DelimitedFiles
using Base.Threads
using TimerOutputs

function Dx(vs)
    1im*k.*vs
end

function Lap(vs)
    -k.^2 .* vs
end

function compute_energy(PsiS)
    PsiSx = Dx(PsiS)
    PsiPx = plan_cr*PsiSx
    PsiP = plan_cr*PsiS
    Den = abs.(PsiP).^2
    E = c^2/(rho0*N) * sum(xi^2*abs.(PsiPx).^2 + Den.^2/(2*rho0) - Den )
    return E
end

