using FFTW
using Base.Threads
using TimerOutputs

function Euler!(PsiS, Psinew, dt)
    PsiS .= PsiS + dt*Psinew
end

function Euler!(PsiS, dt; timer=TimerOutput())
    Psinew = @timeit timer "RHS" RHS(PsiS; timer=timer)
    PsiS .= PsiS + dt*Psinew
end

function Euler(PsiS, Psinew, dt)
    PsiS + dt*Psinew
end

function RK2!(PsiS, dt; timer=TimerOutput())
    PsiS1 = @timeit timer "RHS" RHS(PsiS; timer=timer)
    PsiS2 = @timeit timer "RHS" RHS(PsiS .+ PsiS1*dt/2; timer=timer)
    PsiS .= PsiS + dt * PsiS2
end

function RK4!(PsiS, PsiS1, PsiS2, PsiS3, PsiS4, dt; timer=TimerOutput())
    PsiS1 .= @timeit timer "RHS" RHS(PsiS; timer=timer)
    PsiS2 .= @timeit timer "RHS" RHS(PsiS .+ PsiS1*dt/2; timer=timer)
    PsiS3 .= @timeit timer "RHS" RHS(PsiS .+ PsiS2*dt/2; timer=timer)
    PsiS4 .= @timeit timer "RHS" RHS(PsiS .+ PsiS3*dt; timer=timer)
    PsiS  .= PsiS + dt / 6 * (PsiS1 + 2*PsiS2 + 2*PsiS3 + PsiS4)
end

function RK4!(PsiS, dt; timer=TimerOutput())
    PsiS1 = @timeit timer "RHS" RHS(PsiS; timer=timer)
    PsiS2 = @timeit timer "RHS" RHS(PsiS .+ PsiS1*dt/2; timer=timer)
    PsiS3 = @timeit timer "RHS" RHS(PsiS .+ PsiS2*dt/2; timer=timer)
    PsiS4 = @timeit timer "RHS" RHS(PsiS .+ PsiS3*dt; timer=timer)
    PsiS .= PsiS + dt / 6 * (PsiS1 + 2*PsiS2 + 2*PsiS3 + PsiS4)
end
