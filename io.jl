using FFTW
using DelimitedFiles
using Base.Threads
using TimerOutputs

function SaveOutputs(vs, n, nsave)
    v = plan_cr*vs
    ReaPsi = real(v)
    ImaPsi = imag(v)
    open(path_out*"time.txt","a") do io
        println(io,t[n+1])
    end
    open(path_out*"ReaPsi_"*lpad(n÷nsave,3,"0")*".txt","w") do io
        writedlm(io,ReaPsi, Float64)
    end
    open(path_out*"ImaPsi_"*lpad(n÷nsave,3,"0")*".txt","w") do io
        writedlm(io,ImaPsi, Float64)
    end
end

function SaveEnergy(E)
    open(path_out*"energy.txt","a") do io
        println(io,E)
    end
end
